# podcasting

[![Built with Nix](https://img.shields.io/static/v1?label=built%20with&message=nix&color=5277C3&logo=nixos&style=flat-square&logoColor=ffffff)](https://builtwithnix.org)
[![Crates](https://img.shields.io/crates/v/fyyd-api?style=flat-square)](https://crates.io/crates/podcasting)
[![Documentation](https://img.shields.io/badge/podcasting-documentation-fc0060?style=flat-square)](https://docs.rs/podcasting)
[![Namespace](https://img.shields.io/static/v1?label=Podcasting%20&message=2.0&color=ff1c41&logo=rss&style=flat-square&logoColor=ffffff)](https://podcastindex.org/namespace/1.0)


Rust bindings to the podcasting 2.0 xml types.
Currently only the stable definitions are supported.

## Usage
```
podcasting = "0"
```

or use `cargo add`:

```
cargo add podcasting
```

## Limitations

The library currently fits my needs, but is still limited, notably:

- The WIP types are not defined yet.
- There are no examples yet.

If you have different needs, or find improvements, 
I am always happy about contributions.
Please see the contributing section for that.

## Contributing
[How to contribute.](./docs/CONTRIBUTING.md)

## Reference
- [Podcasting Documentation](https://podcastindex-org.github.io/docs-api/)
- [Podcasting Schema](https://github.com/Podcastindex-org/podcast-namespace/tree/main/docs/schema)

## License
MIT
