use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Root {
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "schema")]
    pub xs_schema: XsSchema,
}

#[derive(Serialize, Deserialize)]
pub struct XsSchema {
    #[serde(rename = "@xmlns:xs")]
    pub xmlns_xs: String,
    #[serde(rename = "@elementFormDefault")]
    pub element_form_default: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "simpleType")]
    pub xs_simple_type: Vec<XsSchemaXsSimpleType>,
    #[serde(rename = "element")]
    pub xs_element: Vec<XsMaxLengthXsElement>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSchemaXsSimpleType {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "restriction")]
    pub xs_restriction: XsSchemaXsSimpleTypeXsRestriction,
}

#[derive(Serialize, Deserialize)]
pub struct XsSchemaXsSimpleTypeXsRestriction {
    #[serde(rename = "@base")]
    pub base: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "enumeration")]
    pub xs_enumeration: Vec<XsEnumeration>,
    #[serde(rename = "maxLength")]
    pub xs_max_length: Option<XsMaxLength>,
}

#[derive(Serialize, Deserialize)]
pub struct XsEnumeration {
    #[serde(rename = "@value")]
    pub value: String,
}

#[derive(Serialize, Deserialize)]
pub struct XsMaxLength {
    #[serde(rename = "@value")]
    pub value: String,
}

#[derive(Serialize, Deserialize)]
pub struct XsMaxLengthXsElement {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "complexType")]
    pub xs_complex_type: XsMaxLengthXsElementXsComplexType,
    #[serde(rename = "annotation")]
    pub xs_annotation: Option<XsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotationXsAttributeXsAnnotationXsAnnotation>,
}

#[derive(Serialize, Deserialize)]
pub struct XsMaxLengthXsElementXsComplexType {
    #[serde(rename = "@mixed")]
    pub mixed: Option<String>,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "simpleContent")]
    pub xs_simple_content: Option<XsSimpleContent>,
    #[serde(rename = "sequence")]
    pub xs_sequence: Option<XsSequence>,
    #[serde(rename = "attribute")]
    pub xs_attribute: Vec<XsComplexTypeXsAttributeXsAnnotationXsAttribute>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSimpleContent {
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "extension")]
    pub xs_extension: XsExtension,
}

#[derive(Serialize, Deserialize)]
pub struct XsExtension {
    #[serde(rename = "@base")]
    pub base: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "annotation")]
    pub xs_annotation: Option<XsSimpleTypeXsRestrictionXsEnumerationXsMaxLengthXsElementXsComplexTypeXsSimpleContentXsExtensionXsAnnotation>,
    #[serde(rename = "attribute")]
    pub xs_attribute: Vec<XsSimpleContentXsExtensionXsAnnotationXsAttribute>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSimpleTypeXsRestrictionXsEnumerationXsMaxLengthXsElementXsComplexTypeXsSimpleContentXsExtensionXsAnnotation
{
    #[serde(rename = "documentation")]
    pub xs_documentation: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSimpleContentXsExtensionXsAnnotationXsAttribute {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@use")]
    pub xs_attribute_use: String,
    #[serde(rename = "@type")]
    pub xs_attribute_type: Option<String>,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "annotation")]
    pub xs_annotation: XsEnumerationXsMaxLengthXsElementXsComplexTypeXsSimpleContentXsExtensionXsAnnotationXsAttributeXsAnnotation,
    #[serde(rename = "simpleType")]
    pub xs_simple_type: Option<XsAnnotationXsSimpleType>,
}

#[derive(Serialize, Deserialize)]
pub struct XsEnumerationXsMaxLengthXsElementXsComplexTypeXsSimpleContentXsExtensionXsAnnotationXsAttributeXsAnnotation
{
    #[serde(rename = "documentation")]
    pub xs_documentation: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct XsAnnotationXsSimpleType {
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "restriction")]
    pub xs_restriction: XsAnnotationXsSimpleTypeXsRestriction,
}

#[derive(Serialize, Deserialize)]
pub struct XsAnnotationXsSimpleTypeXsRestriction {
    #[serde(rename = "@base")]
    pub base: String,
}

#[derive(Serialize, Deserialize)]
pub struct XsSequence {
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "element")]
    pub xs_element: XsSequenceXsElement,
}

#[derive(Serialize, Deserialize)]
pub struct XsSequenceXsElement {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@minOccurs")]
    pub min_occurs: String,
    #[serde(rename = "@maxOccurs")]
    pub max_occurs: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "complexType")]
    pub xs_complex_type: XsSequenceXsElementXsComplexType,
}

#[derive(Serialize, Deserialize)]
pub struct XsSequenceXsElementXsComplexType {
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "attribute")]
    pub xs_attribute: Vec<XsSequenceXsElementXsComplexTypeXsAttribute>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSequenceXsElementXsComplexTypeXsAttribute {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@use")]
    pub xs_attribute_use: Option<String>,
    #[serde(rename = "@type")]
    pub xs_attribute_type: Option<String>,
    #[serde(rename = "@default")]
    pub default: Option<String>,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "annotation")]
    pub xs_annotation: XsAttributeXsAnnotationXsSimpleTypeXsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotation,
}

#[derive(Serialize, Deserialize)]
pub struct XsAttributeXsAnnotationXsSimpleTypeXsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotation
{
    #[serde(rename = "documentation")]
    pub xs_documentation: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct XsComplexTypeXsAttributeXsAnnotationXsAttribute {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@use")]
    pub xs_attribute_use: String,
    #[serde(rename = "@type")]
    pub xs_attribute_type: Option<String>,
    #[serde(rename = "$text")]
    pub text: Option<String>,
    #[serde(rename = "annotation")]
    pub xs_annotation: Option<XsSimpleTypeXsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotationXsAttributeXsAnnotation>,
}

#[derive(Serialize, Deserialize)]
pub struct XsSimpleTypeXsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotationXsAttributeXsAnnotation
{
    #[serde(rename = "documentation")]
    pub xs_documentation: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct XsRestrictionXsSequenceXsElementXsComplexTypeXsAttributeXsAnnotationXsAttributeXsAnnotationXsAnnotation
{
    #[serde(rename = "documentation")]
    pub xs_documentation: String,
    #[serde(rename = "$text")]
    pub text: Option<String>,
}
